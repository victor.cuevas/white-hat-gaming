import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Game } from '../../modules/core/models/game';
import { RestApiService } from '../../modules/core/services/rest-api.service';
import { repeat, Subject, takeUntil } from 'rxjs';
import { Jackpot } from '../../modules/core/models/jackpot';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridComponent implements OnDestroy {
  gamesList: Game[] = this.route.snapshot.parent?.data['categories'].gamesList;
  jackpotsList: Map<string, number> = this.route.snapshot.parent?.data['categories'].jackpotsMap;

  filteredList: Game[] = this.gamesList;

  categoryName = this.route.snapshot.params['categoryName'];

  private hostDestroyed$: Subject<void> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private restApiService: RestApiService,
    private cd: ChangeDetectorRef
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.filterGameList();
    this.updateJackpot();
  }

  ngOnDestroy() {
    this.hostDestroyed$.next();
    this.hostDestroyed$.complete();
  }

  private updateJackpot(): void {
    this.restApiService
      .getJackpotList()
      .pipe(repeat({ delay: 10000 }), takeUntil(this.hostDestroyed$))
      .subscribe((data) => {
        const jackpotsMap: Map<string, number> = new Map();
        data.forEach((jackpot: Jackpot) => jackpotsMap.set(jackpot.game, jackpot.amount));
        this.jackpotsList = jackpotsMap;
        this.cd.detectChanges();
      });
  }

  private filterGameList(): void {
    if (this.categoryName && this.categoryName !== 'other') {
      this.filteredList = this.gamesList.filter((game: Game) => {
        const categoryFound = game.categories.filter((category: string) => {
          return category === this.categoryName;
        });
        return (categoryFound.length && categoryFound) || null;
      });
    } else if (this.categoryName === 'other') {
      this.filteredList = this.gamesList.filter((game: Game) => {
        const categoryFound = game.categories.filter((category: string) => {
          return category === 'ball' || category === 'virtual' || category === 'fun';
        });
        return (categoryFound.length && categoryFound) || null;
      });
    }
  }
}
