import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { KeyValue } from '@angular/common';
import { UtilsService } from '../../modules/core/services/utils.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  categories: Map<string, string> = this.route.snapshot.data['categories'].categoriesMap;

  constructor(private route: ActivatedRoute) {}

  sortCategories(a: KeyValue<string, string>, b: KeyValue<string, string>): number {
    return UtilsService.sortCategories(a, b);
  }
}
