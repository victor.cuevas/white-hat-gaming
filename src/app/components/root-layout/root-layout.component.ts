import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from '../../modules/shared/animations/slide-in.animation';

@Component({
  selector: 'app-root-layout',
  templateUrl: './root-layout.component.html',
  styleUrls: ['./root-layout.component.scss'],
  animations: [slideInAnimation],
})
export class RootLayoutComponent {
  public prepareRoute(outlet: RouterOutlet) {
    console.log(outlet?.activatedRouteData?.['animation']);
    return outlet?.activatedRouteData?.['animation'];
  }
}
