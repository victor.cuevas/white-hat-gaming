import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Jackpot } from '../models/jackpot';
import { Game } from '../models/game';

@Injectable({
  providedIn: 'root',
})
export class RestApiService {
  constructor(private http: HttpClient) {}

  public getGamesList(): Observable<Game[]> {
    return this.http.get<Game[]>('http://stage.whgstage.com/front-end-test/games.php');
  }

  public getJackpotList(): Observable<Jackpot[]> {
    return this.http.get<Jackpot[]>('http://stage.whgstage.com/front-end-test/jackpots.php');
  }
}
