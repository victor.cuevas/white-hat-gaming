import { Injectable } from '@angular/core';
import { KeyValue } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  static sortCategories(a: KeyValue<string, string>, b: KeyValue<string, string>) {
    if (a.value === 'top' || b.value === 'top') {
      return a.value === 'top' ? -1 : 1;
    }
    if (a.value === 'new' || b.value === 'new') {
      return a.value === 'new' ? -1 : 1;
    }
    return 0;
  }
}
