import { TestBed } from '@angular/core/testing';

import { UtilsService } from './utils.service';
import { TestHelperModule } from '../../test-helper/test-helper.module';

describe('UtilsService', () => {
  let service: UtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [TestHelperModule] });
    service = TestBed.inject(UtilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should sort inputB as first item', () => {
    const inputA = { key: 'test', value: 'unit' };
    const inputB = { key: 'test2', value: 'new' };
    const output = UtilsService.sortCategories(inputA, inputB);
    expect(output).toEqual(1);
  });

  it('should sort inputA as first item', () => {
    const inputA = { key: 'test', value: 'top' };
    const inputB = { key: 'test2', value: 'new' };
    const output = UtilsService.sortCategories(inputA, inputB);
    expect(output).toEqual(-1);
  });

  it('should sort inputB as first item', () => {
    const inputA = { key: 'test2', value: 'new' };
    const inputB = { key: 'test', value: 'top' };
    const output = UtilsService.sortCategories(inputA, inputB);
    expect(output).toEqual(1);
  });

  it('should leave items as is', () => {
    const inputA = { key: 'test', value: 'bb' };
    const inputB = { key: 'test2', value: 'aa' };
    const output = UtilsService.sortCategories(inputA, inputB);
    expect(output).toEqual(0);
  });
});
