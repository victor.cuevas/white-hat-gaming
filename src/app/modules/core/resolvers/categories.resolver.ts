import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { delay, forkJoin, map, Observable } from 'rxjs';
import { RestApiService } from '../services/rest-api.service';
import { Game } from '../models/game';
import { Jackpot } from '../models/jackpot';

@Injectable({
  providedIn: 'root',
})
export class CategoriesResolver
  implements
    Resolve<{
      categoriesMap: Map<string, string>;
      gamesList: Game[];
      jackpotsMap: Map<string, number>;
    }>
{
  constructor(private restApiService: RestApiService) {}

  resolve(): Observable<{
    categoriesMap: Map<string, string>;
    gamesList: Game[];
    jackpotsMap: Map<string, number>;
  }> {
    return forkJoin([
      this.restApiService.getGamesList(),
      this.restApiService.getJackpotList(),
    ]).pipe(
      delay(3000),
      map(([gamesList, jackpotsList]: [Game[], Jackpot[]]) => {
        const categoriesMap: Map<string, string> = new Map();
        const jackpotsMap: Map<string, number> = new Map();

        this.generateCategories(categoriesMap, gamesList);

        jackpotsList.forEach((jackpot: Jackpot) => jackpotsMap.set(jackpot.game, jackpot.amount));

        return { categoriesMap, gamesList, jackpotsMap };
      })
    );
  }

  private generateCategories(categoriesMap: Map<string, string>, gamesList: Game[]) {
    gamesList.forEach((game: Game) =>
      game.categories.forEach((category: string) => {
        const categoryPath = category;
        let categoryLabel = category;
        if (category.toLowerCase() === 'new' || category.toLowerCase() === 'top') {
          categoryLabel += ' games';
        }
        categoriesMap.set(categoryLabel, categoryPath);
      })
    );
    categoriesMap.delete('ball');
    categoriesMap.delete('virtual');
    categoriesMap.delete('fun');
    categoriesMap.set('other', 'other');
  }
}
