import { TestBed } from '@angular/core/testing';

import { CategoriesResolver } from './categories.resolver';
import { TestHelperModule } from '../../test-helper/test-helper.module';
import { Game } from '../models/game';

describe('CategoriesResolver', () => {
  let resolver: CategoriesResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [TestHelperModule] });
    resolver = TestBed.inject(CategoriesResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should test category map setting other', () => {
    const gamesList: Game[] = [];
    const categoriesMap = new Map<string, string>();
    resolver['generateCategories'](categoriesMap, gamesList);
    expect(categoriesMap.size).toEqual(1);
  });

  it('should test category map setting more than one', () => {
    const gamesList: Game[] = [
      { categories: ['new', 'top', 'test'], name: 'testName', image: 'testImage', id: 'testId' },
    ];
    const expectedCategories = ['new', 'top', 'test', 'other'];
    const categoriesMap = new Map<string, string>();
    resolver['generateCategories'](categoriesMap, gamesList);

    expect(categoriesMap.size).toEqual(expectedCategories.length);
  });

  it('should test category map setting name modifications', () => {
    const gamesList: Game[] = [
      { categories: ['new', 'top'], name: 'testName', image: 'testImage', id: 'testId' },
    ];
    const expectedCategories = ['new games', 'top games', 'other'];
    const categoriesMap = new Map<string, string>();
    resolver['generateCategories'](categoriesMap, gamesList);

    expect(categoriesMap.size).toEqual(expectedCategories.length);
    expect(categoriesMap.get(expectedCategories[0])).toEqual('new');
    expect(categoriesMap.get(expectedCategories[1])).toEqual('top');
  });
});
