import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientTestingModule, RouterTestingModule],
})
export class TestHelperModule {}
