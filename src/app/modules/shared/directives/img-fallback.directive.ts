import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appImgFallback]',
})
export class ImgFallbackDirective {
  constructor(private elementRef: ElementRef) {}

  @HostListener('error')
  loadFallbackImageOnError() {
    const img: HTMLImageElement = <HTMLImageElement>this.elementRef.nativeElement;
    img.src = 'assets/images/white_hat_gaming_logo.198x120.png';
  }
}
