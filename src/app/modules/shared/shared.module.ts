import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../../components/header/header.component';
import { GridComponent } from '../../components/grid/grid.component';
import { RootLayoutComponent } from '../../components/root-layout/root-layout.component';
import { RouterModule } from '@angular/router';
import { ImgFallbackDirective } from './directives/img-fallback.directive';

@NgModule({
  declarations: [HeaderComponent, GridComponent, RootLayoutComponent, ImgFallbackDirective],
  exports: [HeaderComponent, GridComponent, RootLayoutComponent],
  imports: [CommonModule, RouterModule],
})
export class SharedModule {}
