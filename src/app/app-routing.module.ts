import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesResolver } from './modules/core/resolvers/categories.resolver';
import { GridComponent } from './components/grid/grid.component';
import { RootLayoutComponent } from './components/root-layout/root-layout.component';

const routes: Routes = [
  {
    path: '',
    component: RootLayoutComponent,
    resolve: { categories: CategoriesResolver },
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: GridComponent,
      },
      {
        path: ':categoryName',
        component: GridComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
